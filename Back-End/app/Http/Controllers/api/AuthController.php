<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public function LoginActivity(){
        if(Auth::attempt(['email' =>request('email') ,'password' =>request('password')])){
            $users = Auth::user();
            $success['token'] = $users->createToken('App')->accessToken;
            return response()->json(['success'=>$success, 'users' => $users], 200);

        }else{
            return response()->json(['error'=> 'Data yang Dimasukan Salah'], 401);
        }
    }
    public function RegisterActivity(Request $request){
        $validator = $request->validate([
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:6'
        ]);

        $dataUser = new User;
        $dataUser->name = $request->name;
        $dataUser->email = $request->email;
        $dataUser->password = bcrypt($request->password);
        $dataUser->save();
        
        $token = $dataUser->createToken('App')->accessToken;
        $username = $dataUser->name;

        return response()->json([
            'token'=>$token,
            'name'=>$username
        ],200);
    }
    public function logout(Request $request){
        $token = $request-user()->$token();
        $token->revoke();
        return response()->json([
            'message'=>'Logout'
        ],200);
    }
    public function CaptchaVerify(Request $request){
        $token = $request->token;
        $client = new \GuzzleHttp\Client();
        $client->post(
        'https://www.google.com/recaptcha/api/siteverify ',
        array(
        'form_params' => array(
            'secret' => '6Ldha9QUAAAAABOYZqCj9fCkNG4t_rQgDOUokjPU',
            'response' => $token,
        )
        )
);
$contents = $client->getBody()->getContents();
    }
}
